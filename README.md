# Mini projrct 6

Instrument a Rust Lambda Function with Logging and Tracing

## Requiements

Add logging to a Rust Lambda function

Integrate AWS X-Ray tracing

Connect logs/traces to CloudWatch

## Steps

### Environment setup

1. Create a rust lambda function

   ```
   cargo lambda new project6
   ```
2. Write code in src/main.rs. This function will take one input arguement *input_string* and output its lowercase.n Also, logging and tracing are added to this Rust Lambda function.

```
use lambda_runtime::{service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use tracing_subscriber::fmt::format::FmtSpan;
use tracing::{info, instrument};

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_span_events(FmtSpan::CLOSE)
        .init();

    let func = service_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

#[derive(Debug, Deserialize)]
struct CustomEvent {
    input_string: String,
}

#[derive(Serialize)]
struct CustomOutput {
    uppercase_string: String,
}

#[instrument]
async fn func(event: LambdaEvent<CustomEvent>) -> Result<CustomOutput, Error> {
    let (event, _context) = event.into_parts();
    info!("Received event: {:?}", event);
    let uppercase_string = event.input_string.to_uppercase();
    Ok(CustomOutput { uppercase_string })
}
```
3. Build, test and deploy this Lambda function.
```
cargo lambda build
cargo lambda deploy
```
4. Turn on **AWS X-Ray** and **CloudWatch Lambda Insights** in AWS Console.

![](aws_console.png)


## Test & Screenshot Display

1. Test in AWS Console. As shown in the figure below, this function runs successfully.
![](./aws_test.png)


2. X-Ray traces
![img](./xray.png)

3. Trace details
![img](./detail.png)
